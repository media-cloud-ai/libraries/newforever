extern crate clap;
extern crate serde_derive;

use bytes::Bytes;
use clap::{App, Arg};
use futures::channel::mpsc::channel;
use futures_util::TryStreamExt;
use log::{debug, error, info};
use rs_newforever::{DecoderInput, EncoderOutput, SubtitlesDecoder, SubtitlesEncoder};
use srt_tokio::SrtSocket;
use std::{
  collections::VecDeque,
  io::{Error, ErrorKind, Result},
  thread,
  time::Duration,
};
use tokio::runtime::Runtime;

pub async fn read_packets(srt_socket: &mut SrtSocket) -> Result<Bytes> {
  match srt_socket.try_next().await {
    Ok(Some((instant, bytes))) => {
      debug!("Packet instant: {:?}", instant);
      debug!("Packet data: {:?}", &bytes);
      Ok(bytes)
    }
    Ok(None) => {
      debug!("Packet without data, connection timeout ?");
      Err(Error::new(ErrorKind::Other, "Packet without data"))
    }
    Err(message) => {
      error!("{:?}", message);
      Err(message)
    }
  }
}

#[tokio::main]
async fn main() -> Result<()> {
  let matches = App::new("rs-newforever")
    .version("1.0")
    .about("Listen SRT WebVTT stream and stream teletext in newfor")
    .arg(
      Arg::with_name("listener")
        .short("l")
        .long("listener")
        .value_name("listener")
        .help("Listener mode")
        .takes_value(false),
    )
    .arg(
      Arg::with_name("url")
        .short("u")
        .long("url")
        .value_name("URL")
        .help("URL")
        .default_value("localhost")
        .takes_value(true),
    )
    .arg(
      Arg::with_name("port")
        .short("p")
        .long("port")
        .value_name("PORT")
        .help("Port")
        .default_value("8999")
        .takes_value(true),
    )
    .arg(
      Arg::with_name("output")
        .short("o")
        .long("output")
        .value_name("OUTPUT")
        .help("Output URL")
        .default_value("./output.txt")
        .takes_value(true),
    )
    .arg(
      Arg::with_name("v")
        .short("v")
        .multiple(true)
        .help("Sets the level of verbosity"),
    )
    .get_matches();

  match matches.occurrences_of("v") {
    0 => simple_logger::init_with_level(log::Level::Error).unwrap(),
    1 => simple_logger::init_with_level(log::Level::Info).unwrap(),
    2 => simple_logger::init_with_level(log::Level::Debug).unwrap(),
    _ => simple_logger::init_with_level(log::Level::Trace).unwrap(),
  }

  let listener = matches.is_present("listener");
  let port = matches.value_of("port").unwrap().parse::<u16>().unwrap();
  let output = matches
    .value_of("output")
    .unwrap()
    .parse::<String>()
    .unwrap();

  let mut srt_socket = if listener {
    info!("Wait for connection..");
    SrtSocket::builder().listen_on(port).await?
  } else {
    let url = matches.value_of("url").unwrap().parse::<String>().unwrap();
    let uri = format!("{}:{}", url, port);
    info!("Try connection on {}", uri);
    SrtSocket::builder().call(&uri[..], None).await?
  };

  info!("Stream hooked");

  let (mut source_sender, mut source_receiver) = channel(10000);

  // Spawn a thread handling ffmpeg stuff
  let _ffmpeg_thread = Some(thread::spawn(move || {
    let future = async {
      info!("thread receiving");

      let webvtt_input_stream: VecDeque<u8> = VecDeque::new();
      let webvtt_input = DecoderInput::Stream {
        input_stream: &webvtt_input_stream,
        format_name: "webvtt",
      };

      let ttml_output = EncoderOutput::Url {
        url: &output,
        format: Some("teletext".to_string()),
      };

      unsafe {
        let decoder = SubtitlesDecoder::new(&webvtt_input, "webvtt", 10).unwrap();
        let mut encoder =
          SubtitlesEncoder::new(&ttml_output, decoder.style_header(), "teletext", 1024).unwrap();

        loop {
          match source_receiver.try_next() {
            Ok(Some(result)) => {
              let bytes: Bytes = result;
              let mut webvtt_input_stream: VecDeque<u8> = VecDeque::new();
              let mut buf: VecDeque<u8> = bytes.to_vec().into();
              webvtt_input_stream.append(&mut buf);

              let webvtt_input = DecoderInput::Stream {
                input_stream: &webvtt_input_stream,
                format_name: "webvtt",
              };

              let mut decoder = SubtitlesDecoder::new(&webvtt_input, "webvtt", 10).unwrap();

              while let Some(subtitle) = decoder.decode_next_frame().unwrap() {
                encoder.encode(subtitle).unwrap();
              }

              while let Some(subtitle) = decoder.flush().unwrap() {
                encoder.encode(subtitle).unwrap();
              }
            }
            Ok(None) => {
              error!("Channel closed");
            }
            Err(_error) => {
              // error!("Error: {}", error);
            }
          }
        }
      };
    };

    let runtime = Runtime::new().unwrap();

    runtime.block_on(future);
  }));

  let mut sended;

  loop {
    sended = false;
    let bytes = read_packets(&mut srt_socket).await?;
    while !sended {
      match source_sender.try_send(bytes.clone()) {
        Ok(_) => {
          sended = true;
        }
        Err(error) => {
          if error.is_full() {
            error!("Buffer is full!");
            thread::sleep(Duration::from_millis(50));
          }
          if error.is_disconnected() {
            return Err(Error::new(ErrorKind::Other, "Disconnected"));
          }
        }
      }
    }
  }
}
