#[macro_use]
extern crate serde_derive;

use bytes::Bytes;
use futures::channel::mpsc::{channel, Sender};
use mcai_worker_sdk::{
  default_rust_mcai_worker_description, job::JobResult, prelude::webvtt::webvtt_vec_to_string,
  prelude::*, MessageError,
};
use rs_newforever::{
  DecoderInput, EncoderOutput, InputFormat, OutputFormat, SubtitlesDecoder, SubtitlesEncoder,
};

use std::{
  collections::VecDeque,
  str::FromStr,
  sync::{mpsc::Sender as StdSender, Arc, Mutex},
  thread,
  thread::{sleep, JoinHandle},
  time::Duration,
};
use tokio::runtime::Runtime;

default_rust_mcai_worker_description!();

#[derive(Debug, Default)]
struct McaiRustWorker {}

#[derive(Debug, Default)]
#[allow(dead_code)]
struct TranscodeEvent {
  data_source_sender: Option<Sender<Bytes>>,
  input_data_format: InputFormat,
  output_data_format: OutputFormat,
  sender: Option<Arc<Mutex<StdSender<ProcessResult>>>>,
  ffmpeg_thread: Option<JoinHandle<()>>,
}

#[derive(Debug, Clone, Deserialize, JsonSchema)]
#[allow(dead_code)]
pub struct WorkerParameters {
  /// # Input Format
  /// Input Format
  input_format: Option<String>,
  /// # Output Format
  /// Output Format
  output_format: Option<String>,
  /// # Output URI
  /// Output URI
  output_uri: Option<String>,
  destination_path: String,
  source_path: String,
}

impl McaiWorker<WorkerParameters, RustMcaiWorkerDescription> for TranscodeEvent {
  fn init_process(
    &mut self,
    parameters: WorkerParameters,
    _format_context: Arc<Mutex<FormatContext>>,
    _response_sender: Arc<Mutex<StdSender<ProcessResult>>>,
  ) -> Result<Vec<StreamDescriptor>> {
    let param_input_format = parameters.input_format.clone().unwrap();
    let param_output_format = parameters.output_format.clone().unwrap();
    let output = parameters.output_uri.unwrap();

    // Specify input format
    self.input_data_format = InputFormat::from_str(
      &(parameters
        .input_format
        .unwrap_or_else(|| InputFormat::WebVtt.to_string())),
    )
    .expect("Cannot get input format");

    // Specify output format
    self.output_data_format = OutputFormat::from_str(
      &(parameters
        .output_format
        .unwrap_or_else(|| OutputFormat::Teletext.to_string())),
    )
    .expect("Cannot get output format");

    let (data_source_sender, mut data_source_receiver) = channel(10000);
    self.data_source_sender = Some(data_source_sender);

    // Spawn a thread with ffmpeg handling
    self.ffmpeg_thread = Some(thread::spawn(move || {
      let future = async {
        info!("thread receiving");

        let ttml_output = EncoderOutput::Url {
          url: &output,
          format: Some(param_output_format.clone()),
        };

        let mut encoder = {
          let webvtt_input_stream: VecDeque<u8> = VecDeque::new();
          let webvtt_input = DecoderInput::Stream {
            input_stream: &webvtt_input_stream,
            format_name: &param_input_format,
          };
          let decoder =
            unsafe { SubtitlesDecoder::new(&webvtt_input, &param_input_format, 10).unwrap() };

          let result = retry::retry(retry::delay::Fixed::from_millis(2000).take(3), || {
            match unsafe {
              SubtitlesEncoder::new(
                &ttml_output,
                decoder.style_header(),
                &param_output_format,
                1024,
              )
            } {
              Ok(result) => Ok(result),
              Err(err) => {
                error!("Error: {}", err);
                Err(err)
              }
            }
          });

          if result.is_err() {
            error!("Error: Cannot connect after 3 attemps, aborting.");
            panic!();
          }

          result.unwrap()
        };

        loop {
          match data_source_receiver.try_next() {
            Ok(Some(result)) => {
              let bytes: Bytes = result;
              let mut webvtt_input_stream: VecDeque<u8> = VecDeque::new();
              let mut buf: VecDeque<u8> = bytes.to_vec().into();
              webvtt_input_stream.append(&mut buf);

              let webvtt_input = DecoderInput::Stream {
                input_stream: &webvtt_input_stream,
                format_name: &param_input_format,
              };

              let mut decoder =
                unsafe { SubtitlesDecoder::new(&webvtt_input, &param_input_format, 10).unwrap() };

              while let Some(subtitle) = unsafe { decoder.decode_next_frame().unwrap() } {
                if let Err(err) = unsafe { encoder.encode(subtitle) } {
                  error!("Error: {}", err);
                  panic!();
                }
              }

              while let Some(subtitle) = unsafe { decoder.flush().unwrap() } {
                if let Err(err) = unsafe { encoder.encode(subtitle) } {
                  error!("Error: {}", err);
                  panic!();
                }
              }
            }
            Ok(None) => {
              error!("Channel closed");
            }
            Err(_error) => {
              // error!("Error: {}", error);
            }
          }
        }
      };

      let runtime = Runtime::new().unwrap();

      runtime.block_on(future);
    }));

    match self.input_data_format {
      InputFormat::WebVtt => Ok(vec![StreamDescriptor::new_webvtt(0)]),
    }
  }

  fn process_frames(
    &mut self,
    job_result: JobResult,
    _stream_index: usize,
    process_frames: &[mcai_worker_sdk::prelude::ProcessFrame],
  ) -> Result<ProcessResult> {
    let _input_data = &self.input_data_format; // Might wanna check interop type between process_frame and input_data
    let output_data = &self.output_data_format;
    let process_frame = &process_frames[0];
    match (&process_frame, output_data) {
      (ProcessFrame::WebVtt(frame), OutputFormat::Teletext) => {
        let webvtt_vec = (**frame).clone();
        let mut it = webvtt_vec.iter().peekable();
        while let Some(webvtt) = it.next() {
          let exposition_time: u64 =
            (webvtt.end.to_milliseconds() - webvtt.begin.to_milliseconds()) as u64;
          info!("WebVTT chunk: {:#?}", webvtt);
          let bytes: Bytes = webvtt_vec_to_string(vec![webvtt.clone()], false, false)
            .unwrap()
            .into();
          let mut sended: bool = false;
          while !sended {
            if let Some(data_source_sender) = &mut self.data_source_sender {
              match data_source_sender.try_send(bytes.clone()) {
                Ok(_) => {
                  sended = true;
                }
                Err(error) => {
                  if error.is_full() {
                    error!("Buffer is full!");
                    thread::sleep(Duration::from_millis(50));
                  }
                  if error.is_disconnected() {
                    return Err(MessageError::ProcessingError(
                      job_result
                        .clone()
                        .with_status(JobStatus::Error)
                        .with_message("Disconnected"),
                    ));
                  }
                }
              }
            }
          }
          if it.peek().is_some() {
            sleep(Duration::from_millis(exposition_time));
          }
        }
      }
      (_, _) => {
        return Err(MessageError::ProcessingError(
          job_result
            .clone()
            .with_status(JobStatus::Error)
            .with_message("Unsupported transition"),
        ));
      }
    }

    Ok(ProcessResult::empty())
  }

  fn ending_process(&mut self) -> Result<()> {
    self.ffmpeg_thread.take().map(JoinHandle::join);
    Ok(())
  }
}

fn main() {
  let worker = TranscodeEvent::default();
  start_worker(worker);
}
