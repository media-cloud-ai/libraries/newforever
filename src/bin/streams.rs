use rs_newforever::{DecoderInput, EncoderOutput, SubtitlesDecoder, SubtitlesEncoder};
use std::io::Cursor;

fn main() {
  let content = include_str!("../../tests/hls/0.vtt");

  let webvtt_input_stream = Cursor::new(content.as_bytes().to_vec());
  let webvtt_input = DecoderInput::Stream {
    input_stream: &webvtt_input_stream,
    format_name: "webvtt",
  };

  let mut ttml_output_stream = Cursor::new(Vec::<u8>::new());
  let ttml_output = EncoderOutput::Stream {
    output_stream: &ttml_output_stream,
    format_name: "ttml",
  };

  unsafe {
    let mut decoder = SubtitlesDecoder::new(&webvtt_input, "webvtt", 10).unwrap();
    let mut encoder =
      SubtitlesEncoder::new(&ttml_output, decoder.style_header(), "ttml", 1024).unwrap();

    let subtitles = decoder.decode_all().unwrap();
    println!("subtitles: {subtitles:?}");
    encoder.encode_all(subtitles).unwrap();
  };

  // Display output
  let mut ttml_content = String::new();
  ttml_output_stream.set_position(0);
  std::io::Read::read_to_string(&mut ttml_output_stream, &mut ttml_content).unwrap();

  println!("### Output:\n{ttml_content}");
}
