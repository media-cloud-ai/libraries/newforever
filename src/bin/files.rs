use rs_newforever::{DecoderInput, EncoderOutput, SubtitlesDecoder, SubtitlesEncoder};

fn main() {
  let output_file_path = "./test_files.ttml";
  let webvtt_input_url = DecoderInput::Url {
    url: "./tests/hls/0.vtt",
    format: None,
  };
  let ttml_output_url = EncoderOutput::Url {
    url: output_file_path,
    format: None,
  };

  if std::fs::remove_file(output_file_path).is_ok() {
    println!("### Removed existing  {output_file_path} file.");
  }

  unsafe {
    let mut decoder = SubtitlesDecoder::new(&webvtt_input_url, "webvtt", 10).unwrap();
    let mut encoder =
      SubtitlesEncoder::new(&ttml_output_url, decoder.style_header(), "ttml", 1024).unwrap();

    while let Some(subtitle) = decoder.decode_next_frame().unwrap() {
      encoder.encode(subtitle).unwrap();
    }

    while let Some(subtitle) = decoder.flush().unwrap() {
      encoder.encode(subtitle).unwrap();
    }

    encoder.close().unwrap()
  };

  // Display output
  let ttml_content = std::fs::read_to_string("./test_files.ttml").unwrap();
  println!("### {output_file_path} file content:\n{ttml_content}");
}
