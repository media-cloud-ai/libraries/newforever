use rs_newforever::{DecoderInput, EncoderOutput, SubtitlesDecoder, SubtitlesEncoder};

fn main() {
  let webvtt_input_url = DecoderInput::Url {
    url: "./tests/hls/0.vtt",
    format: None,
  };
  let ttml_output_url = EncoderOutput::Url {
    url: "udp://127.0.0.1:3333",
    format: Some("mpegts".to_string()),
  };

  unsafe {
    let mut decoder = SubtitlesDecoder::new(&webvtt_input_url, "webvtt", 10).unwrap();
    let mut encoder =
      SubtitlesEncoder::new(&ttml_output_url, decoder.style_header(), "ttml", 1024).unwrap();

    let subtitles = decoder.decode_all().unwrap();
    encoder.encode_all(subtitles).unwrap();
  }
}
