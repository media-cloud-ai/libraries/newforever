use std::io::Read;

pub enum DecoderInput<'a> {
  Stream {
    input_stream: &'a dyn Read,
    format_name: &'a str,
  },
  Url {
    url: &'a str,
    format: Option<String>,
  },
}
