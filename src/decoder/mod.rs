mod decoder_context;
mod decoder_input;

use self::decoder_context::SubtitlesDecoderContext;
use crate::{check_result, tools, Result, SubtitlesStyleHeader};
use ffmpeg_sys_next::*;
use log::debug;
use std::{
  ffi::{c_char, c_int, c_void, CString},
  ptr::null_mut,
};

pub use decoder_input::DecoderInput;

pub struct SubtitlesDecoder {
  context: SubtitlesDecoderContext,
  style_header: SubtitlesStyleHeader,
}

unsafe extern "C" fn read_data(opaque: *mut c_void, raw_buffer: *mut u8, buf_size: i32) -> i32 {
  let mut input_stream: &mut &mut dyn std::io::Read = &mut *(opaque as *mut &mut dyn std::io::Read);

  let buffer = std::slice::from_raw_parts_mut(raw_buffer, buf_size as usize);

  let size = std::io::Read::read(&mut input_stream, buffer).unwrap();
  debug!("[SubtitlesDecoder] Read {size} bytes.");

  size as i32
}

impl SubtitlesDecoder {
  /// # Safety
  ///
  /// Unsafe function as FFmpeg FFI
  pub unsafe fn new(input: &DecoderInput, codec_name: &str, buffer_size: usize) -> Result<Self> {
    let mut context = SubtitlesDecoderContext::default();
    let codec_name = CString::new(codec_name)?;
    match input {
      DecoderInput::Url {
        url: input_url,
        format,
      } => {
        debug!("[SubtitlesDecoder] Open {input_url} URL...");
        let input_url = CString::new(<&str>::clone(input_url))?;

        context.format_context = avformat_alloc_context();

        if let Some(input_format) = format {
          let input_format = CString::new(input_format.as_str())?;
          let av_input_format = av_find_input_format(input_format.as_ptr());

          check_result!(avformat_open_input(
            &mut context.format_context,
            input_url.as_ptr(),
            av_input_format,
            null_mut(),
          ));
        } else {
          check_result!(avformat_open_input(
            &mut context.format_context,
            input_url.as_ptr(),
            null_mut(),
            null_mut(),
          ));
        }

        let codec_d = avcodec_find_decoder_by_name(codec_name.as_ptr());
        context.codec_context = avcodec_alloc_context3(codec_d);

        check_result!(avcodec_parameters_to_context(
          context.codec_context,
          (**(*context.format_context).streams.offset(0)).codecpar
        ));

        (*context.codec_context).time_base = AVRational { num: 1, den: 1 };
        (*context.codec_context).pkt_timebase = AVRational { num: 1, den: 1 };

        check_result!(avcodec_open2(context.codec_context, codec_d, null_mut()));

        let style_header = SubtitlesStyleHeader::new(&tools::to_string(
          (*context.codec_context).subtitle_header as *const c_char,
        ));

        context.io_context = null_mut();

        Ok(Self {
          context,
          style_header,
        })
      }
      DecoderInput::Stream {
        input_stream,
        format_name,
      } => {
        let format_name = CString::new(<&str>::clone(format_name))?;

        debug!("[SubtitlesDecoder] Open stream...");
        let buffer = av_malloc(buffer_size);

        context.format_context = avformat_alloc_context();
        let av_input_format_d = av_find_input_format(format_name.as_ptr());

        let opaque = Box::new(<&dyn std::io::Read>::clone(input_stream));
        context.io_context = avio_alloc_context(
          buffer as *mut u8,
          buffer_size as i32,
          0,
          Box::into_raw(opaque) as *mut c_void,
          Some(read_data),
          None,
          None,
        );

        (*context.format_context).pb = context.io_context;

        check_result!(avformat_open_input(
          &mut context.format_context,
          null_mut(),
          av_input_format_d,
          null_mut(),
        ));

        let codec_d = avcodec_find_decoder_by_name(codec_name.as_ptr());
        context.codec_context = avcodec_alloc_context3(codec_d);

        check_result!(avcodec_parameters_to_context(
          context.codec_context,
          (**(*context.format_context).streams.offset(0)).codecpar
        ));

        (*context.codec_context).time_base = AVRational { num: 1, den: 1 };
        (*context.codec_context).pkt_timebase = AVRational { num: 1, den: 1 };

        check_result!(avcodec_open2(context.codec_context, codec_d, null_mut()));

        let style_header = SubtitlesStyleHeader::new(&tools::to_string(
          (*context.codec_context).subtitle_header as *const c_char,
        ));

        Ok(Self {
          context,
          style_header,
        })
      }
    }
  }

  pub fn style_header(&self) -> &SubtitlesStyleHeader {
    &self.style_header
  }

  /// # Safety
  ///
  /// Unsafe function as FFmpeg FFI
  pub unsafe fn decode_next_frame(&mut self) -> Result<Option<AVSubtitle>> {
    let ret = av_read_frame(self.context.format_context, self.context.packet);
    if ret < 0 {
      debug!("[SubtitlesDecoder] {}", tools::error_to_string(ret));
      return Ok(None);
    }

    let subtitle = AVSubtitle {
      format: 0,
      start_display_time: 0,
      end_display_time: 0,
      num_rects: 0,
      rects: null_mut(),
      pts: 0,
    };

    let got_sub: c_int = 0;

    check_result!(avcodec_decode_subtitle2(
      self.context.codec_context,
      &subtitle as *const AVSubtitle as *mut _,
      &got_sub as *const i32 as *mut _,
      self.context.packet,
    ));

    if got_sub == 0 {
      debug!("[SubtitlesDecoder] Unable to decode subtitle");
      return Ok(None);
    }

    Ok(Some(subtitle))
  }

  /// # Safety
  ///
  /// Unsafe function as FFmpeg FFI
  pub unsafe fn flush(&mut self) -> Result<Option<AVSubtitle>> {
    debug!("[SubtitlesDecoder] Flush decoder...");

    (*self.context.packet).data = null_mut();
    (*self.context.packet).size = 0;

    let subtitle = AVSubtitle {
      format: 0,
      start_display_time: 0,
      end_display_time: 0,
      num_rects: 0,
      rects: null_mut(),
      pts: 0,
    };

    let got_sub: c_int = 0;

    check_result!(avcodec_decode_subtitle2(
      self.context.codec_context,
      &subtitle as *const AVSubtitle as *mut _,
      &got_sub as *const i32 as *mut _,
      self.context.packet,
    ));

    if got_sub == 0 {
      return Ok(None);
    }

    Ok(Some(subtitle))
  }

  /// # Safety
  ///
  /// Unsafe function as FFmpeg FFI
  pub unsafe fn decode_all(&mut self) -> Result<Vec<AVSubtitle>> {
    let mut subtitles = vec![];

    debug!("[SubtitlesDecoder] Decode stream...");
    while let Some(subtitle) = self.decode_next_frame()? {
      subtitles.push(subtitle);
    }

    // flush decoder
    while let Some(subtitle) = self.flush()? {
      subtitles.push(subtitle);
    }

    Ok(subtitles)
  }
}
