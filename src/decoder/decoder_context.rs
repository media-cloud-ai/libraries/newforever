use ffmpeg_sys_next::*;
use std::ptr::null_mut;

#[repr(C)]
pub struct SubtitlesDecoderContext {
  pub format_context: *mut AVFormatContext,
  pub codec_context: *mut AVCodecContext,
  pub io_context: *mut AVIOContext,
  pub packet: *mut AVPacket,
}

impl Default for SubtitlesDecoderContext {
  fn default() -> Self {
    unsafe {
      let av_packet = av_packet_alloc();
      av_init_packet(av_packet);

      Self {
        format_context: null_mut(),
        codec_context: null_mut(),
        io_context: null_mut(),
        packet: av_packet,
      }
    }
  }
}

impl Drop for SubtitlesDecoderContext {
  fn drop(&mut self) {
    unsafe {
      av_packet_free(&mut self.packet);
      avcodec_free_context(&mut self.codec_context);
      avio_context_free(&mut self.io_context);
      avformat_free_context(self.format_context);
    }
  }
}
