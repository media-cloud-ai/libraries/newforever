pub struct SubtitlesStyleHeader(String);

impl SubtitlesStyleHeader {
  pub fn new(content: &str) -> Self {
    Self(content.to_owned())
  }

  pub fn content(&self) -> &str {
    &self.0
  }

  pub fn size(&self) -> usize {
    self.0.len()
  }
}
