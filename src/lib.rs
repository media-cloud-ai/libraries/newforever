mod decoder;
mod encoder;
mod error;
mod format;
mod subtitles_style_header;

pub use decoder::{DecoderInput, SubtitlesDecoder};
pub use encoder::{EncoderOutput, SubtitlesEncoder};
pub use error::{Error, Result};
pub use format::{InputFormat, OutputFormat};
pub use subtitles_style_header::SubtitlesStyleHeader;

mod tools {
  use ffmpeg_sys_next::{av_strerror, AV_ERROR_MAX_STRING_SIZE};
  use std::ffi::{c_char, c_int, CStr};
  use std::str::from_utf8_unchecked;

  pub unsafe fn to_string(data: *const c_char) -> String {
    if data.is_null() {
      return "".to_string();
    }
    from_utf8_unchecked(CStr::from_ptr(data).to_bytes()).to_string()
  }

  pub unsafe fn error_to_string(errnum: c_int) -> String {
    let mut data = [0; AV_ERROR_MAX_STRING_SIZE];
    av_strerror(errnum, data.as_mut_ptr(), AV_ERROR_MAX_STRING_SIZE);
    to_string(data.as_ptr())
  }
}

#[macro_export]
macro_rules! check_result {
  ($condition: expr) => {
    let errnum = $condition;
    if errnum < 0 {
      let error = $crate::tools::error_to_string(errnum);
      return Err($crate::error::Error::FFmpegError(errnum, error));
    }
  };
}
