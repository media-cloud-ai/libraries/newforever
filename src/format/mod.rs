use mcai_worker_sdk::prelude::*;
use serde::Deserialize;
use std::str::FromStr;

#[derive(Clone, Debug, Default, Deserialize, JsonSchema, PartialEq, Eq)]
pub enum InputFormat {
  #[default]
  WebVtt,
}

impl FromStr for InputFormat {
  type Err = MessageError;

  fn from_str(input: &str) -> Result<InputFormat> {
    match input {
      "webvtt" => Ok(InputFormat::WebVtt),
      _ => Err(MessageError::RuntimeError(format!(
        "Invalid Enum: {:?}",
        input
      ))),
    }
  }
}

impl ToString for InputFormat {
  fn to_string(&self) -> String {
    match &self {
      InputFormat::WebVtt => "webvtt".to_string(),
    }
  }
}

#[derive(Debug, Default, Clone, Deserialize)]
pub enum OutputFormat {
  #[default]
  Teletext,
}

impl FromStr for OutputFormat {
  type Err = mcai_worker_sdk::MessageError;

  fn from_str(input: &str) -> Result<OutputFormat> {
    match input {
      "teletext" => Ok(OutputFormat::Teletext),
      _ => {
        warn!("Unknwon output format, falling back to Teletext");
        Ok(OutputFormat::Teletext)
      }
    }
  }
}

impl ToString for OutputFormat {
  fn to_string(&self) -> String {
    match &self {
      OutputFormat::Teletext => "teletext".to_string(),
    }
  }
}
