use std::ffi::NulError;
use std::fmt::{Debug, Display, Formatter};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, Clone)]
pub enum Error {
  CStringAlloc(NulError),
  FFmpegError(i32, String),
  UninitializedDecoder,
  UninitializedEncoder,
}

impl From<NulError> for Error {
  fn from(error: NulError) -> Self {
    Self::CStringAlloc(error)
  }
}

impl Display for Error {
  fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
    match self {
      Self::CStringAlloc(error) => write!(f, "{error:?}"),
      Self::FFmpegError(err_num, message) => write!(f, "{message} {err_num}"),
      Self::UninitializedDecoder => f.write_str("Decoder not initialized."),
      Self::UninitializedEncoder => f.write_str("Encoder not initialized."),
    }
  }
}

impl std::error::Error for Error {}
