use self::encoder_context::SubtitlesEncoderContext;
use crate::{check_result, Result, SubtitlesStyleHeader};
use ffmpeg_sys_next::*;
use log::debug;
use std::ffi::{c_int, c_void, CString};
use std::io::Write;
use std::ptr::{null, null_mut};

mod encoder_context;
mod encoder_output;

pub use encoder_output::EncoderOutput;

#[derive(Default)]
pub struct SubtitlesEncoder {
  context: SubtitlesEncoderContext,
}

unsafe extern "C" fn write_data(opaque: *mut c_void, raw_buffer: *mut u8, buf_size: i32) -> i32 {
  let mut output_stream: &mut &mut dyn Write = &mut *(opaque as *mut &mut dyn Write);

  let buffer = std::slice::from_raw_parts_mut(raw_buffer, buf_size as usize);
  let size = std::io::Write::write(&mut output_stream, buffer).unwrap();
  debug!("[SubtitlesEncoder] Wrote {size} bytes");

  size as i32
}

impl SubtitlesEncoder {
  /// # Safety
  ///
  /// Unsafe function as FFmpeg FFI
  pub unsafe fn new(
    output: &EncoderOutput,
    style_header: &SubtitlesStyleHeader,
    codec_name: &str,
    buffer_size: usize,
  ) -> Result<Self> {
    let mut context = SubtitlesEncoderContext::default();

    match output {
      EncoderOutput::Url {
        url: output_url,
        format,
      } => {
        debug!("[SubtitlesEncoder] Open {output_url} URL...");
        let output_url = CString::new(<&str>::clone(output_url))?;

        if let Some(output_format) = format {
          let output_format = CString::new(output_format.as_str())?;
          check_result!(avformat_alloc_output_context2(
            &mut context.format_context,
            null_mut(),
            output_format.as_ptr(),
            output_url.as_ptr()
          ));
        } else {
          check_result!(avformat_alloc_output_context2(
            &mut context.format_context,
            null_mut(),
            null(),
            output_url.as_ptr()
          ));
        }

        context.open_encoder(codec_name, style_header)?;

        check_result!(avio_open(
          &mut (*context.format_context).pb,
          output_url.as_ptr(),
          AVIO_FLAG_WRITE,
        ));

        context.buffer_size = buffer_size as c_int;
      }
      EncoderOutput::Stream {
        output_stream,
        format_name,
      } => {
        debug!("[SubtitlesEncoder] Open {format_name} stream...");
        let format_name = CString::new(<&str>::clone(format_name))?;

        check_result!(avformat_alloc_output_context2(
          &mut context.format_context,
          null_mut(),
          format_name.as_ptr(),
          null()
        ));

        context.open_encoder(codec_name, style_header)?;

        context.buffer_size = buffer_size as c_int;
        context.buffer = av_malloc(buffer_size);

        let opaque = Box::new(<&dyn std::io::Write>::clone(output_stream));
        let avio_context = avio_alloc_context(
          context.buffer as *mut u8,
          context.buffer_size,
          1,
          Box::into_raw(opaque) as *mut c_void,
          None,
          Some(write_data),
          None,
        );

        (*context.format_context).pb = avio_context;
      }
    };

    check_result!(avformat_write_header(context.format_context, null_mut()));

    Ok(Self { context })
  }

  /// # Safety
  ///
  /// Unsafe function as FFmpeg FFI
  pub unsafe fn encode(&mut self, subtitle: AVSubtitle) -> Result<()> {
    debug!("[SubtitleEncoder] encode subtitle: {subtitle:?}");
    let av_packet = self.context.packet;

    let mut buffer = vec![0u8; self.context.buffer_size as usize];

    let size = avcodec_encode_subtitle(
      self.context.codec_context,
      buffer.as_mut_ptr(),
      self.context.buffer_size,
      &subtitle as *const AVSubtitle as *mut _,
    );

    check_result!(size);

    av_packet_unref(av_packet);
    (*av_packet).data = buffer.as_mut_ptr();
    (*av_packet).size = size;
    (*av_packet).pts = av_rescale_q(
      subtitle.pts,
      AV_TIME_BASE_Q,
      (*self.context.codec_context).time_base,
    );
    (*av_packet).duration = av_rescale_q(
      subtitle.end_display_time as i64,
      AVRational { num: 1, den: 1000 },
      (*self.context.codec_context).time_base,
    );
    (*av_packet).dts = (*av_packet).pts;

    check_result!(av_interleaved_write_frame(
      self.context.format_context,
      av_packet
    ));

    Ok(())
  }

  /// # Safety
  ///
  /// Unsafe function as FFmpeg FFI
  pub unsafe fn close(&mut self) -> Result<()> {
    debug!("[SubtitlesEncoder] Write trailer...");
    check_result!(av_write_trailer(self.context.format_context));

    Ok(())
  }

  /// # Safety
  ///
  /// Unsafe function as FFmpeg FFI
  pub unsafe fn encode_all(&mut self, subtitles: Vec<AVSubtitle>) -> Result<()> {
    debug!("[SubtitlesEncoder] Encode subtitles...");

    for sub in subtitles {
      self.encode(sub)?;
    }

    self.close()
  }
}
