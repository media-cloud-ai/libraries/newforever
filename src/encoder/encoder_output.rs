use std::io::Write;

pub enum EncoderOutput<'a> {
  Stream {
    output_stream: &'a dyn Write,
    format_name: &'a str,
  },
  Url {
    url: &'a str,
    format: Option<String>,
  },
}
