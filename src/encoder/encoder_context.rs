use crate::{check_result, Result, SubtitlesStyleHeader};
use ffmpeg_sys_next::*;
use std::ffi::{c_int, c_void, CString};
use std::ptr::{null, null_mut};

#[repr(C)]
pub struct SubtitlesEncoderContext {
  pub format_context: *mut AVFormatContext,
  pub codec_context: *mut AVCodecContext,
  pub packet: *mut AVPacket,
  pub buffer: *mut c_void,
  pub buffer_size: c_int,
}

impl SubtitlesEncoderContext {
  pub fn open_encoder(
    &mut self,
    codec_name: &str,
    style_header: &SubtitlesStyleHeader,
  ) -> Result<()> {
    unsafe {
      let codec_name = CString::new(codec_name)?;
      let stream = avformat_new_stream(self.format_context, null());
      let codec_e = avcodec_find_encoder_by_name(codec_name.as_ptr());
      self.codec_context = avcodec_alloc_context3(codec_e);
      (*self.codec_context).time_base = AVRational { num: 1, den: 1 };

      // Init ASS style header
      let header = CString::new(style_header.content())?;
      (*self.codec_context).subtitle_header_size = style_header.size() as c_int;
      (*self.codec_context).subtitle_header = header.into_raw() as *mut u8;

      check_result!(avcodec_open2(self.codec_context, codec_e, null_mut()));
      check_result!(avcodec_parameters_from_context(
        (*stream).codecpar,
        self.codec_context
      ));
    }
    Ok(())
  }
}

impl Default for SubtitlesEncoderContext {
  fn default() -> Self {
    unsafe {
      let av_packet = av_packet_alloc();
      av_init_packet(av_packet);

      Self {
        format_context: null_mut(),
        codec_context: null_mut(),
        packet: av_packet,
        buffer: null_mut(),
        buffer_size: 0,
      }
    }
  }
}

impl Drop for SubtitlesEncoderContext {
  fn drop(&mut self) {
    unsafe {
      av_free(self.buffer);
      av_packet_free(&mut self.packet);
      avcodec_free_context(&mut self.codec_context);
      avformat_free_context(self.format_context);
    }
  }
}
