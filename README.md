# Newforever

This repo houses wrappers around FFmpeg featuring newfor (teletext) capabilities. Included in wrappers is a MCAI worker.

## Worker

Only performs at the moment WebVTT -> newfor conversion and casting.
Should be moved to a proper project at some point.

## FFmpeg standalone

### Build

```
docker build -t newforever .
docker-slim build --http-probe=false newforever
```

Create archive:
```
docker save newforever.slim:latest | gzip > newforever_latest.tar.gz
docker import newforever_latest.tar.gz
```

### Run

Any input to newfor e.g.

```
docker run -p 9000:9000 -v $PWD:/opt/newforever/ -it newforever:latest http://xxx/pl.m3u8 127.0.0.1:9000
```
or

```
docker run -p 9000:9000 -v $PWD:/opt/newforever/ -it newforever:latest tests/hls/subs_0.vtt 127.0.0.1:9000
```

## Input Format

Sample in the ```tests``` subfolder.

https://www.w3.org/TR/webvtt1/#styling

Balises de position:

```
00:00:00.000 --> 00:00:03.785 position:10%,line-left align:left size:35%
ce phénomène avec Florent Boutet.

Au cœur de la rédaction ?

```

Balises de couleur:

```
WEBVTT

STYLE
::cue(b) {
  color: blue;
}

00:00:00.000 --> 00:00:03.785
<b>ce phénomène avec Florent Boutet.

 Au cœur de la rédaction ?</b>



 WEBVTT
#CLOCK:2022-10-24T08:59:34.276473961+00:00

168
00:00:00.000 --> 00:00:03.785
ce phénomène avec Florent Boutet.
 Au cœur de la rédaction ?

169
00:00:04.650 --> 00:00:05.765
La politique et

170
00:00:05.765 --> 00:00:07.662
le gouvernement face au motion

171
00:00:07.662 --> 00:00:08.180
de censure

172
00:00:08.180 --> 00:00:10.240
cet après-midi à l'Assemblée
 nationale,

173
00:00:10.240 --> 00:00:10.280
la U
```
