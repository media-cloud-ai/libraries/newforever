FROM ubuntu:focal

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Etc/UTC

# hadolint ignore=DL3008, DL3009
RUN apt-get update && apt-get install -y --no-install-recommends ca-certificates \
    pkg-config \
    git wget curl build-essential \
    libtool libtool-bin yasm \
    libass-dev libssl-dev clang

WORKDIR /opt/newforever/

RUN git clone https://github.com/rbouqueau/ffmpeg.git

WORKDIR /opt/newforever/ffmpeg/

# hadolint ignore=SC2046
RUN git checkout aa9b03f1c383eb0d66a4944672e369c384fe2a05 && \
    ./configure --enable-libass && make -j $(nproc) && make install

WORKDIR /opt/newforever/

RUN rm -rf ffmpeg

COPY Cargo.toml /opt/newforever
COPY build.rs /opt/newforever
COPY src /opt/newforever/src
COPY examples /opt/newforever/examples
COPY tests /opt/newforever/tests

RUN curl https://sh.rustup.rs -sSf | \
    sh -s -- --default-toolchain stable -y && \
    . $HOME/.cargo/env && \
    cargo build --bin rs_newforever --verbose --release && \
    cargo install --bin rs_newforever --path . && \
    cp /root/.cargo/bin/rs_newforever /usr/bin && \
    cargo clean

RUN apt-get purge -y --auto-remove git wget build-essential yasm && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV AMQP_QUEUE job_newforever
CMD rs_newforever
